/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: [
            'scontent-fra3-1.xx.fbcdn.net',
            'pbs.twimg.com',
            'static.dezeen.com',
            'upload.wikimedia.org'
        ],
    },
    reactStrictMode:true
};

export default nextConfig;
