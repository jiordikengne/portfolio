import {LaptopIcon, MoonIcon, SunIcon} from "lucide-react";
import {useState} from "react";
import {Button} from "@/components/ui/button";
import {
    DropdownMenu,
    DropdownMenuContent, DropdownMenuItem,
    DropdownMenuTrigger
} from "@/components/ui/dropdown-menu";

const themeOptions: { value: 'system' | 'dark' | 'light', label: string, icon: JSX.Element }[] = [
    { value: 'system', label: 'System', icon: <LaptopIcon className={" h-4 w-4"} size={16} /> },
    { value: 'dark', label: 'Dark', icon: <MoonIcon className={"h-4 w-4"} size={16} /> },
    { value: 'light', label: 'Light', icon: <SunIcon className={"h-4 w-4"} size={16} /> },
];

export const DropDownMenu =()=>{
    const [theme, setTheme] = useState('system');
    /*const [showDropdown, setShowDropdown] = useState(false);*/

    const handleThemeChange = (newTheme: 'system' | 'dark' | 'light') => {
        setTheme(newTheme);
        if (newTheme === 'system') {
            if (matchMedia('(prefers-color-scheme: dark)').matches) {
                document.documentElement.classList.add('dark');
            } else {
                document.documentElement.classList.add('light');
            }
        } else {
            document.documentElement.classList.remove('dark', 'light');
            if (newTheme === 'dark') {
                document.documentElement.classList.add('dark');
            } else if (newTheme === 'light') {
                document.documentElement.classList.add('light');
            }
        }
    };

return (
    <DropdownMenu>
        <DropdownMenuTrigger asChild>
            <Button variant="outline"
                    aria-label="Customise options">
                {themeOptions.find((option) => option.value === theme)?.icon ?? <MoonIcon size={16}/>}
            </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent  className=" rounded-md p-[5px] shadow-[0px_10px_38px_-10px_rgba(22,_23,_24,_0.35),_0px_10px_20px_-15px_rgba(22,_23,_24,_0.2)] will-change-[opacity,transform] data-[side=top]:animate-slideDownAndFade data-[side=right]:animate-slideLeftAndFade data-[side=bottom]:animate-slideUpAndFade data-[side=left]:animate-slideRightAndFade"
                              sideOffset={5}>

                {themeOptions.map((option,index) => (
                    <DropdownMenuItem key={index}  className={"text-center"} onClick={() => handleThemeChange(option.value)}>
                        {option.icon}
                        <span>{option.label}</span>
                    </DropdownMenuItem>
                ))
                }

        </DropdownMenuContent>
    </DropdownMenu>
    /*
    <li className="relative">
        <button
            className={cn(buttonVariants({variant: "link"}), "size-6 p-0")}
            onClick={() => setShowDropdown(!showDropdown)}
        >
            {themeOptions.find((option) => option.value === theme)?.icon ?? <MoonIcon size={16}/>}
            <span className="sr-only">Toggle theme</span>
        </button>
        {showDropdown && (
            <ul className={cn("absolute right-0 mt-2  bg-background border border-foreground shadow-md rounded-md transition duration-300", showDropdown ? "block" : "hidden")}
                onClick={handleOutsideClick}>
                {themeOptions.map((option) => (
                    <li key={option.value}
                        className="px-4 py-2 hover:bg-foreground hover:text-background transition duration-300">
                        <button className={"w-full text-center"} onClick={() => handleThemeChange(option.value)}>
                            {option.icon}
                            {/!* <span className="">{option.label}</span>*!/}
                        </button>
                    </li>
                ))}
            </ul>
        )}
    </li>*/
)
}