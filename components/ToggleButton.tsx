'use client'
import React, { useState } from 'react';
import { FiMoon, FiSun } from 'react-icons/fi';
import { useTheme } from 'next-themes';

const ThemeToggleButton = () => {
    const [isDarkMode, setIsDarkMode] = useState(true);
    const { setTheme } = useTheme();

    const toggleTheme = () => {
        setIsDarkMode(!isDarkMode);
        // Vous pouvez également enregistrer la préférence de l'utilisateur dans localStorage ici.
    };

    return (
        <button onClick={toggleTheme} className="fixed max-lg:bottom-12 bottom-5 right-5 max-lg:right-6 p-2 bg-background rounded-xl shadow  shadow-accent ">
            {isDarkMode ? (
                <FiSun onClick={() => setTheme('light')} />
            ) : (
                <FiMoon onClick={() => setTheme('dark')} />
            )}
        </button>
    );
};

export default ThemeToggleButton;
