'use client'
import { Section } from "@/app/_components/Section";
import { buttonVariants } from "@/components/ui/button";
import Link from "next/link";
import { cn } from "@/lib/utils";
import {GitlabIcon, LinkedinIcon, XIcon} from "lucide-react";
import Image from "next/image";
import {DropDownMenu} from "@/components/DropDownMenu";



export const Header = () => {

    return (
        <header className="sticky top-0 py-4 bg-background z-10">
            <Section className="flex items-baseline">
                <Image src="/images/logo.png" width={128} height={100} alt={'Mon logo'} priority={false} style={{ width: "auto", height: "auto" }} className={"w-32"}/>
                <div className="flex-1"/>
                <ul className="flex items-center gap-2">
                    <Link href={"https://gitlab.com/jiordikengne"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                        <GitlabIcon size={16} className="text-foreground"/>
                    </Link>
                    <Link href={"https://twitter.com/jiordi_kengne"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                        <XIcon size={16} className="text-foreground"/>
                    </Link>
                    <Link href={"https://www.linkedin.com/in/jiordi-viera/"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                        <LinkedinIcon size={16} className="text-foreground"/>
                    </Link>
                    <DropDownMenu/>
                </ul>
            </Section>
        </header>
    );
};