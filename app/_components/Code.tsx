import {ComponentPropsWithoutRef} from "react";
import {cn} from "@/lib/utils";
import {className} from "postcss-selector-parser";

export const Code = (props: ComponentPropsWithoutRef<"span">) => {
    return <span {...props}
                 className={cn("bg-accent/30 font-mono transition-colors px-1 inline text-nowrap py-0.5 hover:bg-accent/50 rounded-sm text-primary", className)}/>
}