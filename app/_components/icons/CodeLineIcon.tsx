import {ComponentPropsWithRef} from "react";

export const CodeLineIcon = (props: ComponentPropsWithRef<"svg"> & { size?: number }) => {
    return (
        <svg width={props.size}
             height={props.size}
             viewBox="0 0 256 250"
             preserveAspectRatio="xMidYMid"
             {...props}>
            <rect x="3.5" y="2.5" width="138" height="138" rx="27.5" fill="currentColor"
                  stroke="url(#paint1_linear_845_1443)" stroke-width="5"></rect>
            <g clip-path="url(#clip0_845_1443)">
                <g filter="url(#filter0_d_845_1443)">
                    <path
                        d="M24.8333 0H1V23.8333M24.8333 0V23.8333M24.8333 0H48.6667M24.8333 23.8333H1M24.8333 23.8333H48.6667M24.8333 23.8333V47.6667M1 23.8333V47.6667M48.6667 0V23.8333M48.6667 0H72.5M48.6667 23.8333H72.5M48.6667 23.8333V47.6667M72.5 0V23.8333M72.5 0H96.3333M72.5 23.8333H96.3333M72.5 23.8333V47.6667M96.3333 0V23.8333M96.3333 0H120.167M96.3333 23.8333H120.167M96.3333 23.8333V47.6667M120.167 0V23.8333M120.167 0H144M120.167 23.8333H144M120.167 23.8333V47.6667M144 0V23.8333M144 0H167.833V23.8333M144 23.8333H167.833M144 23.8333V47.6667M167.833 23.8333V47.6667M24.8333 47.6667H1M24.8333 47.6667H48.6667M24.8333 47.6667V71.5M1 47.6667V71.5M48.6667 47.6667H72.5M48.6667 47.6667V71.5M72.5 47.6667H96.3333M72.5 47.6667V71.5M96.3333 47.6667H120.167M96.3333 47.6667V71.5M120.167 47.6667H144M120.167 47.6667V71.5M144 47.6667H167.833M144 47.6667V71.5M167.833 47.6667V71.5M24.8333 71.5H1M24.8333 71.5H48.6667M24.8333 71.5V95.3333M1 71.5V95.3333M48.6667 71.5H72.5M48.6667 71.5V95.3333M72.5 71.5H96.3333M72.5 71.5V95.3333M96.3333 71.5H120.167M96.3333 71.5V95.3333M120.167 71.5H144M120.167 71.5V95.3333M144 71.5H167.833M144 71.5V95.3333M167.833 71.5V95.3333M24.8333 95.3333H1M24.8333 95.3333H48.6667M24.8333 95.3333V119.167M1 95.3333V119.167M48.6667 95.3333H72.5M48.6667 95.3333V119.167M72.5 95.3333H96.3333M72.5 95.3333V119.167M96.3333 95.3333H120.167M96.3333 95.3333V119.167M120.167 95.3333H144M120.167 95.3333V119.167M144 95.3333H167.833M144 95.3333V119.167M167.833 95.3333V119.167M24.8333 119.167H1M24.8333 119.167H48.6667M24.8333 119.167V143M1 119.167V143M48.6667 119.167H72.5M48.6667 119.167V143M72.5 119.167H96.3333M72.5 119.167V143M96.3333 119.167H120.167M96.3333 119.167V143M120.167 119.167H144M120.167 119.167V143M144 119.167H167.833M144 119.167V143M167.833 119.167V143M24.8333 143H1M24.8333 143H48.6667M24.8333 143V166.833M1 143V166.833H24.8333M48.6667 143H72.5M48.6667 143V166.833M72.5 143H96.3333M72.5 143V166.833M96.3333 143H120.167M96.3333 143V166.833M120.167 143H144M120.167 143V166.833M144 143H167.833M144 143V166.833M167.833 143V166.833H144M24.8333 166.833H48.6667M48.6667 166.833H72.5M72.5 166.833H96.3333M96.3333 166.833H120.167M120.167 166.833H144"
                        stroke="url(#paint2_radial_845_1443)" stroke-width="3.5" shape-rendering="crispEdges"></path>
                </g>
            </g>
            <g filter="url(#filter1_d_845_1443)">
                <path
                    d="M57.2465 32.7503C56.4039 31.4338 55.0857 30.5014 53.5725 30.1516C52.0593 29.8019 50.4707 30.0625 49.1449 30.8779C47.8191 31.6933 46.8609 32.9991 46.4744 34.5173C46.0878 36.0355 46.3034 37.6461 47.0753 39.0061L88.7728 108.181C89.1684 108.878 89.6982 109.488 90.3311 109.975C90.964 110.461 91.6871 110.815 92.4576 111.015C93.2282 111.216 94.0306 111.258 94.8176 111.139C95.6046 111.021 96.3601 110.745 97.0396 110.327C97.7191 109.909 98.3088 109.358 98.7739 108.706C99.239 108.054 99.5701 107.315 99.7476 106.532C99.9251 105.749 99.9454 104.937 99.8074 104.146C99.6694 103.355 99.3758 102.6 98.9439 101.925L57.2465 32.7503Z"
                    fill="white"></path>
                <path
                    d="M107.168 34.9147C106.524 34.2817 105.743 33.7979 104.883 33.4988C104.024 33.1997 103.106 33.093 102.199 33.1863C101.292 33.2797 100.417 33.5708 99.6396 34.0384C98.8621 34.5059 98.2011 35.1381 97.7052 35.8884C96.9442 37.0675 96.62 38.4671 96.787 39.8526C96.954 41.2381 97.602 42.5252 98.6224 43.4981L107.519 52.2014C107.978 52.6507 108.342 53.1843 108.591 53.7716C108.839 54.359 108.967 54.9885 108.967 55.6243C108.967 56.26 108.839 56.8896 108.591 57.4769C108.342 58.0642 107.978 58.5978 107.519 59.0471L98.76 67.6155C97.6861 68.6159 97.0034 69.9529 96.8303 71.3949C96.6572 72.8368 97.0046 74.2927 97.8122 75.5099C98.3221 76.232 98.9878 76.8355 99.7622 77.2776C100.536 77.7197 101.401 77.9896 102.293 78.0682C103.186 78.1468 104.085 78.0321 104.927 77.7324C105.77 77.4327 106.535 76.9552 107.168 76.3338L116.156 67.5257C119.376 64.3684 121.184 60.0877 121.184 55.6243C121.184 51.1609 119.376 46.8801 116.156 43.7228L107.168 34.9147Z"
                    fill="white"></path>
                <path
                    d="M38.2244 90.5653C37.7802 90.1159 37.4278 89.5824 37.1874 88.995C36.947 88.4077 36.8233 87.7782 36.8233 87.1424C36.8233 86.5067 36.947 85.8771 37.1874 85.2898C37.4278 84.7025 37.7802 84.1689 38.2244 83.7196L46.7011 75.1661C47.7403 74.1658 48.401 72.8287 48.5685 71.3868C48.7361 69.9448 48.3999 68.489 47.6183 67.2718C47.1248 66.5496 46.4806 65.9462 45.7312 65.5041C44.9819 65.062 44.1457 64.7921 43.282 64.7135C42.4182 64.6349 41.5479 64.7495 40.7327 65.0492C39.9175 65.3489 39.1774 65.8264 38.5646 66.4479L29.866 75.256C26.7502 78.4132 25 82.694 25 87.1574C25 91.6208 26.7502 95.9016 29.866 99.0588L38.5646 107.867C39.188 108.5 39.9434 108.984 40.7754 109.283C41.6074 109.582 42.4949 109.689 43.373 109.595C44.251 109.502 45.0973 109.211 45.8498 108.743C46.6023 108.276 47.242 107.644 47.7219 106.893C48.4583 105.714 48.7721 104.315 48.6104 102.929C48.4488 101.544 47.8217 100.256 46.8343 99.2835L38.2244 90.5653Z"
                    fill="white"></path>
            </g>
            <defs>
                <filter id="filter0_d_845_1443" x="-4.75" y="-1.75" width="178.333" height="178.333"
                        filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"></feFlood>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                   result="hardAlpha"></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" operator="out"></feComposite>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_845_1443"></feBlend>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_845_1443"
                             result="shape"></feBlend>
                </filter>
                <filter id="filter1_d_845_1443" x="0" y="5" width="146.184" height="131.205"
                        filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"></feFlood>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                   result="hardAlpha"></feColorMatrix>
                    <feOffset></feOffset>
                    <feGaussianBlur stdDeviation="12.5"></feGaussianBlur>
                    <feComposite in2="hardAlpha" operator="out"></feComposite>
                    <feColorMatrix type="matrix"
                                   values="0 0 0 0 0.225 0 0 0 0 0.937262 0 0 0 0 1 0 0 0 0.6 0"></feColorMatrix>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_845_1443"></feBlend>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_845_1443"
                             result="shape"></feBlend>
                </filter>
                <linearGradient id="paint0_linear_845_1443" x1="144" y1="0" x2="1" y2="143"
                                gradientUnits="userSpaceOnUse">
                    <stop stopColor="#0C0D0E"></stop>
                    <stop offset="1" stopColor="#222222"></stop>
                </linearGradient>
                <linearGradient id="paint1_linear_845_1443" x1="144" y1="0" x2="1" y2="143"
                                gradientUnits="userSpaceOnUse">
                    <stop stopColor="white"></stop>
                    <stop offset="1" stopColor="#222222"></stop>
                </linearGradient>
                <radialGradient id="paint2_radial_845_1443" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse"
                                gradientTransform="translate(73.5 71.5) rotate(90.3951) scale(72.5017)">
                    <stop stopColor="#1A1A1A"></stop>
                    <stop offset="1" stopColor="#434343" stopOpacity="0"></stop>
                </radialGradient>
                <clipPath id="clip0_845_1443">
                    <rect width="143" height="143" fill="white" transform="translate(1)"></rect>
                </clipPath>
            </defs>
        </svg>

    )
}