"use client"
import {Section} from "@/app/_components/Section";
import {Badge} from "@/components/ui/badge";
import {ContactCard} from "@/app/_components/ContactCard";
import "aos/dist/aos.css"
import {useEffect} from "react";
import AOS from "aos";

export const Contact = () => {
    useEffect(()=>{
        AOS.init()
    })
    return <Section className="flex flex-col items-start gap-4">
        <Badge variant={"outline"}>Contactez-moi</Badge>
        <h2 className="pb-2 text-3xl font-semibold tracking-tight first:mt-0">
            Je serai ravie de travailler avec vous.
        </h2>
        <div data-aos="fade-down"
             data-aos-easing="linear"
             data-aos-duration="1500" className="flex max-md:flex-col w-full gap-4">
            <ContactCard
                url="https://twitter.com/jiordi_kengne" image="https://pbs.twimg.com/profile_images/1690290841997152257/c9YYgfO4_400x400.jpg"
                         mediumImage="https://static.dezeen.com/uploads/2023/07/x-logo-twitter-elon-musk_dezeen_2364_col_0.jpg"
                         name="@jiordi_kengne" description="Twitter"/>
            <ContactCard url="https://www.linkedin.com/in/jiordi-viera" image="/images/jiordi.jpg"
                         mediumImage="https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/LinkedIn_icon.svg/2048px-LinkedIn_icon.svg.png"
                         name="@jiordi_viera" description="Linkedin"/>
            <ContactCard url="https://wa.me/237682786315?text=Bonjour Je viens de visiter votre site" image="/images/jiordi.jpg"
                         mediumImage="/images/whatsapp.png"
                         name="Mon whatsapp" description="Whatsapp"/>

        </div>
    </Section>
}