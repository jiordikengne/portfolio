import {Card} from "@/components/ui/card";
import {ArrowUpRight} from "lucide-react";
import Link from "next/link";
import Image from "next/image";

export const ContactCard = (props: {
    image: string;
    mediumImage: string;
    name: string;
    description: string;
    url: string
}) => {
    return (<Link target={"_blank"} href={props.url} className="w-full ">
            <Card className="p-3 bg-accent/10 hover:bg-accent/30 transition-colors group flex items-center gap-4">
                <div className="relative h-10 w-10">
                    <Image width={40} height={40} src={props.image} alt={props.name} title={props.name} className="h-10 w-10"/>
                    <Image width={16} height={16} src={props.mediumImage} alt={props.mediumImage} title={props.mediumImage}
                         className="h-4 w-4 absolute -bottom-2 -right-1 rounded-full object-contain"/>
                </div>
                <div className="mr-auto">
                    <div className="flex items-center gap-2">
                        <p className="text-lg font-semibold">{props.name}</p>
                    </div>
                    <p className="text-xs text-muted-foreground">{props.description}</p>
                </div>
                <ArrowUpRight className="group-hover:translate-x-2 mr-4 group-hover:-translate-y-2 transition-transform"
                              size={16}/>
            </Card>
        </Link>
    )
}