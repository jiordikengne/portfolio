"use client"
import AOS from 'aos';
import 'aos/dist/aos.css';
import {Section} from "@/app/_components/Section";
import Link from "next/link";
import {Code} from "@/app/_components/Code";
import {useEffect} from "react";
import {LaravelIcon} from "@/app/_components/icons/LaravelIcon";
import {TailwindLogo} from "@/app/_components/icons/TailwindLogo";
import {Gitlab} from "lucide-react";
import Image from "next/image";
import jiordi_bg_remove from '@/public/images/jiordi_black.svg';


export const Hero = () => {
    useEffect(()=>{
AOS.init({})
    })
    return <Section className="flex max-md:flex-col items-start gap-4">
        <div data-aos="fade-right" data-aos-duration="3000" className="flex-[3] flex flex-col gap-2">
            <h2 className="font-caption font-bold text-5xl text-primary">Jiordi Viera</h2>
            <h3 className="text-3xl font-caption"> Développeur Full-stack Web</h3>
            <p className="text-base leading-7">Je suis un developpeur backend aillant travailler sur plusieurs applications{""}
                <Link className="pl-2" href="https://laravel.com" target="_blank">
                    <Code
                        className="inline-flex items-center gap-1"><LaravelIcon className="inline"
                                                                                size={16}/>Laravel</Code>
                </Link>, utilisant{""}
                <Link className="px-2" href="https://twitter.com/jiordiviera" target="_blank">
                    <Code
                        className="inline-flex items-center gap-1"><TailwindLogo className="inline"
                                                                                size={16}/>Tailwind</Code>
                </Link>pour mon interface, utilisant le logiciel de versioning{""}
                <Link className="pl-2" href="https://twitter.com/jiordiviera" target="_blank">
                    <Code
                        className="inline-flex items-center gap-1">
                        <Gitlab height={16} width={16} className="inline"/>Gitlab</Code>
                </Link>
            </p>
        </div>
        <div className="flex-[2] max-md:m-auto self-end ml-auto">
            <Image data-aos="fade-up" data-aos-duration="1000"  sizes={"100vw"} style={{animationDuration: "30s"}}
                src={jiordi_bg_remove} priority placeholder="empty" quality={100}
                className="w-full animate-bounce transition-shadow  h-auto max-w-sm max-md:w-56 " alt="Jiordi image"/>
        </div>
    </Section>
}