"use client"
import {Section} from "@/app/_components/Section";
import {Card} from "@/components/ui/card";
import {
    ALargeSmall,Building,
    LucideIcon,
    SatelliteDishIcon,
    School
} from "lucide-react";
import Link from "next/link";
import {Badge} from "@/components/ui/badge";
import {ContactCard} from "@/app/_components/ContactCard";
import "aos/dist/aos.css"
import AOS from "aos";
import {useEffect} from "react";
import Image from "next/image";

export const Status = () => {
    useEffect(()=>{
        AOS.init({})
    })
    return (<Section className="flex max-md:flex-col items-start gap-4">

        <div data-aos="fade-right" data-aos-duration="3000" className="flex-[3] w-full">
        <Card className="w-full p-4 flex flex-col gap-2">
                <p className="text-lg text-muted-foreground">Mes projets</p>
                <div className="flex flex-col gap-4">
                    {SIDE_PROJECTS.map((project, index) => (
                        <SideProject
                            key={index}
                            logo={project.logo}
                            title={project.title}
                            description={project.description}
                            url={project.url}/>
                    ))}
                </div>
            </Card>
        </div>
        <div data-aos="fade-left" data-aos-duration="3000" className="flex-[2] w-full flex flex-col gap-4">
            <Card className="p-4 flex-1">
                <p className="text-lg text-muted-foreground">Mes projets funs</p>
                <div className="flex flex-col gap-4">
                    {WORKS.map((work, index) => (
                        <Work
                            key={index}
                            {...work}
                        />
                    ))}
                </div>
            </Card>
            <Card className="p-4 flex-1 flex flex-col gap-2">
                <p className="text-lg text-muted-foreground">Contactez-moi.</p>
                <ContactCard
                    url="https://twitter.com/jiordi_kengne" image="https://pbs.twimg.com/profile_images/1690290841997152257/c9YYgfO4_400x400.jpg"
                    mediumImage="https://static.dezeen.com/uploads/2023/07/x-logo-twitter-elon-musk_dezeen_2364_col_0.jpg"
                    name="@jiordi_kengne" description="Twitter"/>
                <ContactCard url="https://www.linkedin.com/in/jiordi-viera/" image="https://pbs.twimg.com/profile_images/1690290841997152257/c9YYgfO4_400x400.jpg"
                             mediumImage="https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/LinkedIn_icon.svg/2048px-LinkedIn_icon.svg.png"
                             name="@jiordiviera" description="Linkedin"/>
            </Card>
        </div>
    </Section>);
};

const SIDE_PROJECTS: SideProjectsProps[] = [
    {
        logo: SatelliteDishIcon,
        title: "Mt-kits",
        description: "Application de vente de kits starlink",
        url: "https://mtkits.evenafro.ca"
    },
    {
        logo: School,
        title: "Itec",
        description: "Site web de presentation, et application des gestions de notes",
        url: "https://itec.con"
    },
    {
        logo: Building,
        title: "QMS",
        description: "Site de prestation de service de l'entreprise QMS",
        url: "/"
    },
    {
        logo: ALargeSmall,
        title: "Maligha",
        description: "Site de prestation de service de l'entreprise Maligha consulting",
        url: "/"
    }
];
type SideProjectsProps = {
    logo: LucideIcon;
    title: string;
    description: string;
    url: string
}
const SideProject = (props: SideProjectsProps) => {

    return (
        <Link href={props.url}
              className="inline-flex items-center gap-4 hover:bg-accent/50 transition-colors p-1 rounded">
            <span className="bg-accent text-accent-foreground p-3 rounded-sm">
                <props.logo size={16}/>
            </span>
            <div>
                <p className="text-lg font-semibold">{props.title}</p>
                <p className="text-sm text-muted-foreground">{props.description}</p>
            </div>
        </Link>
    )
}
const WORKS: WorkProps[] = [
    {
        image: "/images/logo_M.png",
        title: "Mt-kits",
        role: "Concepteur et développeur",
        date: "2024 - Present",
        url: "https://mtkits.evenafro.ca",
        freelance: true
    }
]
type WorkProps = {
    image: string;
    title: string;
    role: string;
    date: string;
    url: string;
    freelance?: boolean
}
const Work = (props: WorkProps) => {
    return (
        <Link href={props.url}
              className="inline-flex items-center gap-4 hover:bg-accent/50 transition-colors p-1 rounded">
            <Image width={56} height={40} src={props.image} alt={props.title}  className="w-14 h-10  object-contain"/>
            <div className="mr-auto">
                <div className="flex items-center gap-2">
                    <p className="text-lg font-semibold text-nowrap">{props.title}</p>
                    {props.freelance && <Badge variant="outline" className={"font-extrabold font-caption bg-primary to-purple-600 text-transparent bg-clip-text"}>Mission</Badge>}
                </div>
                <p className="text-xs text-muted-foreground text-nowrap">{props.role}</p>
            </div>
            <p className="text-xs text-end text-muted-foreground text-nowrap max-lg:text-wrap">{props.date}</p>
        </Link>
    )
}