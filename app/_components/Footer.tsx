import {Section} from "@/app/_components/Section";
import Link from "next/link";
import {cn} from "@/lib/utils";
import {buttonVariants} from "@/components/ui/button";
import {GitlabIcon, LinkedinIcon, XIcon} from "lucide-react";

export const Footer = () => {
    return <footer className="bg-card">
        <Section
            className="py-4 flex justify-between  max-[600px]:items-center max-[600px]:bg-accent min-[600px]:flex-row flex-col">
            <p className="text-muted-foreground text-sm">© copyright 2024 <Link href="https://wa.me/237682786315"
                                                                                target="_blank">Jiordi viera</Link></p>
            {/*<div className="flex-1"/>*/}
            <ul className="flex items-center flex-nowrap gap-2 max-[600px]:pt-2">
                <Link href={"https://gitlab.com/jiordikengne"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                    <GitlabIcon size={16} className="text-foreground"/>
                </Link>
                <Link href={"https://twitter.com/jiordi_kengne"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                    <XIcon size={16} className="text-foreground"/>
                </Link>
                <Link href={"https://www.linkedin.com/in/jiordi-viera/"} target="_blank" className={cn(buttonVariants({variant: "link"}),"size-6 p-0")}>
                    <LinkedinIcon size={16} className="text-foreground"/>
                </Link>
            </ul>
        </Section>
    </footer>
}