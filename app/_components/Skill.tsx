"use client"
import {Section} from "@/app/_components/Section";
import {Badge} from "@/components/ui/badge";
import {Code} from "@/app/_components/Code";
import {useEffect} from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import {TbBrandLaravel, TbBrandNextjs, TbBrandTailwind} from "react-icons/tb";


export const Skills = () => {
    useEffect(()=>{
        AOS.init()
    })
    return <Section className="flex flex-col items-start gap-4">
        <Badge variant={"outline"}>Compétences</Badge>
        <h2 className="pb-2 text-3xl font-semibold tracking-tight first:mt-0">
            J&apos;adore travailler sur...
        </h2>
        <div className="flex max-md:flex-col gap-4">
            <div data-aos="fade-down"
                 data-aos-easing="linear"
                 data-aos-duration="1500" className="flex flex-col gap-2 flex-1">
                <TbBrandLaravel size={42}/>

                <h3 className="text-2xl font-semibold tracking-tight">Laravel</h3>
                <p className="text-sm text-muted-foreground">Mon premier langage de programmation a été le <Code>PHP</Code>. J&apos;utilise également <Code>Laravel</Code> pour implémenter des API.</p>
            </div>

            <div data-aos="fade-up"
                 data-aos-easing="linear"
                 data-aos-duration="1500" className="flex flex-col gap-2 flex-1">
                <TbBrandTailwind size={42}/>
                <h3 className="text-2xl font-semibold tracking-tight">Tailwind</h3>
                <p className="text-sm text-muted-foreground">Je peux créer <u>de belles</u> applications <i>en quelques secondes</i>  en utilisant <Code>TailwindCSS</Code></p>
            </div>
            <div data-aos="fade-down"
                 data-aos-easing="linear"
                 data-aos-duration="1500" className="flex flex-col gap-2 flex-1">
                <TbBrandNextjs size={42}/>

            <h3 className="text-2xl font-semibold tracking-tight">NextJS</h3>
            <p className="text-sm text-muted-foreground">J&apos;utilise <Code>NextJS</Code> pour <u>faire de belles</u> interface dynamiques <i>en quelques secondes</i> pour améliorer <u>l&apos;expérience utilisateur</u></p>
            </div>

        </div>
    </Section>
}