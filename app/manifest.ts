import {MetadataRoute} from "next";

export default function manifest():MetadataRoute.Manifest {
    return {
        name:'Portfolio jiordi',
        short_name:'Portfolio jiordi',
        description:'Dans cette application, vous pourrez apercevoir mes projets et mes compétences',
        start_url:'/',
        display:'standalone',
        background_color:'#000000',
        theme_color:'#000000',

    }
}