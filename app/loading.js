import styles from '../styles/Main.module.css'

export default function Loading() {
    return (
        <div className={`w-screen h-screen flex items-center justify-center bg-background ${styles['loading']}`}>
            <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-purple-500"></div>
        </div>
    );
}
