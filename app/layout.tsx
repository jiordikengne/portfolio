import type {Metadata} from "next";
import {Sora} from "next/font/google";
import "./globals.css";
import {cn} from "@/lib/utils";
import {GeistSans} from 'geist/font/sans';
import {GeistMono} from 'geist/font/mono';
import {Providers} from "@/components/providers";
import {Analytics} from "@vercel/analytics/react";
import React from "react";


const poppins = Sora({
    weight: ["100", "200", "300", "400", "500", "600", "700", "800"],
    subsets: ["latin"],
    variable: "--font-caption"
});

export const metadata: Metadata = {
    title: {
        default: "Mon portfolio",
        template: "%s | Portfolio de Jiordi Viera"
    },
    description: "Qui suis-je? Je me prénomme JIORDI VIERA, je suis un jeune-homme passionné par l'informatique, plus précisement le domaine de la conception et de la programmation. Je suis développeur full-stack web faisant ayant desa compétences en Laravel, TailwindCSS et dans l'apprentissage de NextJS",
    applicationName: "Portfolio jiordi",
    authors: [{name: "Jiordi Viera", url: "https://portfolio-jiordi.vercel.app/"}],
    twitter: {
        site: "@jiordi_kengne",
        creator: "@jiordi_kengne",
        images: new URL('https://portfolio-jiordi.vercel.app')
    },
    appLinks: {
        web: {
            url: "https://portfolio-jiordi.vercel.app/",
            should_fallback: true
        }
    },
    manifest: "/site.webmanifest",
    icons: {
        icon: '/favicon.ico',
        shortcut: '/favicon-16x16.png',
        apple: '/apple-touch-icon.png'
    },
    keywords: ["Jiordi Viera", "Portfolio", "CodeByJiordi", "NextJS", "TailwindCSS", "Laravel", "React", "laravel 11", "IUT DE DOUALA", "IUT", "Informatique"],
    creator: "Jiordi Viera",
    publisher: "Jiordi Viera",
    robots: {
        index: false,
        follow: true,
        nocache: true,
        googleBot: {
            index: true,
            follow: false,
            noimageindex: true,
            "max-video-preview": -1,
            "max-image-preview": 'large',
            "max-snippet": -1,
        },
    },
    category: "portfolio",
    openGraph: {
        title: "Portfolio Jiordi viera",
        images: new URL('https://portfolio-jiordi.vercel.app'),
        countryName: "cm"
    }
};


export default function RootLayout({children}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang={'fr'} suppressHydrationWarning>
        <body
            className={cn(GeistSans.variable, GeistMono.variable, poppins.variable, "overflow-x-hidden mx-auto font-sans h-full bg-background text-foreground")}>
        <link rel="icon" href="../public/favicon.ico"/>
        <Providers>
            {children}
            <Analytics/>
        </Providers>
        </body>
        </html>
    );
}
